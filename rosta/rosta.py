#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
"""
from distutils.dir_util import (copy_tree, remove_tree, \
                                mkpath, DistutilsFileError)
from pathlib import Path
import os

from utils import *
from page import Page


class Site(object):

    def __init__(self):
        self.pages = []
        self.articles = []
        self.categories = []

    def get_all_articles(self):
        # commandline: `init` run once
        post_dir = get_post_path()
        p = Path(post_dir)
        for child in p.iterdir():
            page = Page(child, "post")
            self.pages.append(page)
        self.articles = get_articles_from_pages(self.pages)
        return self.pages, self.articles

    def get_all_categories(self):
        self.categories = category_groupby(self.articles)
        return self.categories

    def generate_cache(self, arg):
        # in vuejs we can use timestamp,so the map in  key=lambda is not necessary
        if arg == "post":
            data = sorted(self.articles, key=lambda k: k["date"], reverse=True)
        elif arg == "category":
            data = self.categories
        cache_dump(arg, data)

    def generate_pagination_pages(self, articles=[]):
        if self.articles:
            articles = self.articles
        elif articles:
            pass
        else:
            articles = cache_read("post")
        pagination_pages(articles)

    def generate_category_pages(self, categories=[]):
        if self.categories:
            categories = self.categories
        elif categories:
            pass
        else:
            categories = cache_read("category")
        category_pages(categories)

    def generate_post_pages(self, pages=[]):
        if self.pages and not pages:
            pages = self.pages
        generate_page(pages)

    def generate_normal_pages(self):
        # index, about in config.USER_CONFIG.pages
        pass

    @staticmethod
    def update_cache(new_articles):
        articles = cache_read("post")
        categories = cache_read("category")
        for article in new_articles:
            articles = insert_article(article, articles)
            key = article["category"]
            for category in categories:
                if category["name"] == key:
                    category["articles"] = insert_article(article, category["articles"])
                    break
        cache_dump("post", articles)
        cache_dump("category", categories)
        return articles, categories

    @staticmethod
    def check_status():
        return files_tobe_commit()

    @staticmethod
    def update_pages(files):
        pages = []
        articles = []
        for file in files:
            if is_post(file):
                page = Page(file, "post")
                article = page.open_article()
                pages.append(page)
                articles.append(article)
            else:
                pages.append(Page(file, "page"))
        return pages, articles

    def load_config(self):
        pass

    @staticmethod
    def create_directory():
        # commandline: `init` run once
        root = get_root_path()
        site = get_site_path()
        cache = os.path.join(root, 'cache')
        data = os.path.join(site, 'data')
        posts = os.path.join(site, 'data', 'post')
        for path in [cache, site, data, posts]:
            try:
                mkpath(path)
            except DistutilsFileError as e:
                print("Cannot create the directory", e)

    @staticmethod
    def create_config(**kw):
        return generate_config(**kw)

    @staticmethod
    def copy_static_files():
        root = get_root_path()
        dst = get_site_path()
        src = os.path.join(root, 'site', 'dist')
        print(src, dst)
        if os.path.exists(dst):
            pass# remove_tree(dst)
        else:
            copy_tree(src, dst)

    @staticmethod
    def render_template():
        generate_app()

    @staticmethod
    def app_build():
        build()

    @staticmethod
    def init_repo(remote=False):
        create_repo(remote)

    @staticmethod
    def commit_repo():
        commit_change()

    @staticmethod
    def publish():
        remote_publish()


# config
"""
input: site-title， 'description'， site-url， author
ouput: config.py by config.jinja
site.create_directory()
copy_static_files(get_root_path(), get_root_path())
"""
# Init put md file in posts
"""
site = Site()
# load_config
site.get_all_articles()
site.get_all_categories()
site.generate_cache("post")
site.generate_cache("category")
site.generate_pagination_pages()
site.generate_post_pages()
site.generate_category_pages()
"""
# update
"""
site = Site()
path = site.check_status()
pages, articles = site.update_pages(path)
articles, categories = site.update_cache(articles)
site.generate_pagination_pages(articles)
site.generate_category_pages(categories)
site.generate_post_pages(pages)
"""
# test
"""
# simple post a article
page = Page('/Users/cesc/playground/rosta/rosta/demo/demo.md', "post")
print(page.generate_html_file())
"""

# -*- coding: utf-8 -*-
"""
获取文章对象，创建html文件
"""
from utils import generate_page_name, generate_page_path, writer, reader
from article import Article
import json


class Page(object):
    """docstring for Page"""
    # type : ["post", "page"] receive "index, about"
    def __init__(self, page_file_path, type):
        super(Page, self).__init__()
        self.page_file_path = page_file_path
        self.type = type
        self.filename = ""

    def open_article(self):
        article = Article(self.page_file_path)
        article_object = article.get_content()
        self.filename = article_object.get('title', self.filename)
        return article_object

    def generate_json_file(self):
        data = self.open_article()
        dst_filename = self.filename
        filename, _ = generate_page_name(dst_filename, self.type)
        filepath = generate_page_path(filename, self.type)
        content = json.dumps(data)
        result = writer(filepath, content)
        if result:
            return filepath

# -*- coding: utf-8 -*-

"""Console script for rosta."""
import os
import sys
import click
from rosta import Site
from utils import get_root_path


@click.group()
def main(args=None):
    """Console script for rosta."""
    click.echo("Replace this message by putting your code into "
               "rosta.cli.main")
    click.echo("See click documentation at http://click.pocoo.org/")
    return 0


@click.command()
def init():
    click.echo('Set the site information and configuration and initialize.')
    site_name = click.prompt('Please enter the name of your site', type=str, default="sample")
    description = click.prompt('Please enter the name of your description', type=str, default="A blog.")
    name = click.prompt("Please enter the name of your name as the article's author", type=str, default="Admin")
    root = get_root_path()
    default_output_path = os.path.join(root, 'site')
    default_origin_path = os.path.join(root, site_name)
    output_path = click.prompt("Please enter the directory of output html files", type=str, default=default_output_path)
    origin_path = click.prompt("Please enter the directory of your post files", type=str, default=default_origin_path)
    if click.confirm('Do you want to publish your pages to git hosts?'):
        remote_url = click.prompt("Please enter the repository url", type=str, default='git@github.com:username/repository.git')
    else:
        remote_url = ''
    Site.create_config(
        site_name=site_name,
        description=description,
        name=name,
        output_path=output_path,
        origin_path=origin_path,
        remote_url=remote_url
    )
    Site.create_directory()
    if remote_url:
        Site.init_repo(True)
    Site.render_template()
    Site.app_build()


@click.command()
def generate():
    click.echo('Initialized the site.')
    site = Site()
    site.get_all_articles()
    site.get_all_categories()
    site.generate_cache("post")
    site.generate_cache("category")
    site.generate_pagination_pages()
    site.generate_post_pages()
    site.generate_category_pages()
    Site.commit_repo()
    # Site.publish()


@click.command()
def update():
    click.echo('Update those modified pages.')
    path = Site.check_status()
    click.echo("Find these files will be compiled and update:")
    click.echo(path)
    if click.confirm('Do you want to update those pages above?'):
        pages, articles = Site.update_pages(path)
        articles, categories = Site.update_cache(articles)
        site = Site()
        site.generate_pagination_pages(articles)
        site.generate_category_pages(categories)
        site.generate_post_pages(pages)
        site.commit_repo()


@click.command()
def publish():
    click.echo('Publish your site to the Git host.')
    Site.publish()


main.add_command(init)
main.add_command(generate)
main.add_command(update)
main.add_command(publish)

if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover

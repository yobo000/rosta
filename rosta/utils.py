# -*- coding: utf-8 -*-
import pickle
from datetime import date
from itertools import groupby
from operator import itemgetter
import subprocess
import shlex
import json
import sys
import os

from git import Repo
import markdown
import jinja2

from config import USER_CONFIG, SITE_DEFAULT_CONFIG, OUTPUT_PATH, ORIGIN_PATH, GIT_REPO


def get_root_path():
    root_dir = os.path.abspath(
        os.path.join(
            os.path.dirname(
                sys.argv[0]), '..'))
    # Py2exe does not provide an __file__ variable.
    # so cannot use os.path.abspath(__file__)
    templates = os.path.join(root_dir, 'templates')
    if os.path.exists(templates):
        return root_dir
    else:
        return None


ROOT = get_root_path()


def get_post_path():
    return os.path.join(ORIGIN_PATH, 'posts')


def get_origin_path():
    return ORIGIN_PATH


def get_site_path():
    return OUTPUT_PATH


def generate_page_name(title, type):
    if type == "post":
        name = '-'.join(title.lower().split(' '))
    elif type == "page":
        name = title
    return name + '.json', name


def generate_page_path(filename, type):
    if type == "post":
        path = os.path.join(OUTPUT_PATH, 'data', type, filename)
    elif type == "page":
        path = os.path.join(OUTPUT_PATH, 'data', filename)
    return path


def compile_article_content(content):
    mardown_generator = markdown.Markdown(extensions=[
        'fenced_code',
        'codehilite',
        'tables',
        'footnotes',
        'meta',
    ])
    generated_html = mardown_generator.convert(content)
    article = parse_article_metadata(mardown_generator)
    article["body"] = generated_html
    return article


def metadata_parser(mardown_generator, keys):
    metadata = {}
    for key in keys:
        value = mardown_generator.Meta[key]
        if len(value) > 1:
            metadata[key] = value
        elif ',' in value[0]:
            metadata[key] = list(map(lambda x: x.strip(), value[0].split(',')))
        else:
            metadata[key] = value[0]
    return metadata


def parse_article_metadata(mardown_generator):
    metadata = {}
    if mardown_generator.Meta:
        meta_list = ['title', 'category', 'author', 'tags', 'layout']
        metadata = metadata_parser(mardown_generator, meta_list)
        """
        metadata['title'] = mardown_generator.Meta['title'][0]
        metadata['category'] = mardown_generator.Meta['category'][0]
        metadata['author'] = mardown_generator.Meta['author'][0]
        metadata['tags'] = mardown_generator.Meta['tags'][0].split(', ')
        metadata['layout'] = mardown_generator.Meta['layout'][0]
        """
        if mardown_generator.Meta.get('date', False):
            date_string = mardown_generator.Meta['date'][0]
        else:
            date_string = date.today().isoformat()
        metadata['date'] = date_string
        _, filename = generate_page_name(metadata['title'], "post")
        if metadata['layout'] == 'post':
            metadata['url'] = '/post/' + filename
    return metadata


def category_groupby(articles):
    result = []
    for key, items in groupby(articles, itemgetter('category')):
        items_sorted = sorted(items, key=itemgetter('date'), reverse=True)
        result.append({"name": key, "articles": items_sorted})
    return result


def cache_dump(arg, content):
    # dump post and category
    if arg in ["post", "category"]:
        path = os.path.join(ROOT, 'cache', arg+'.pkl')
        with open(path, 'wb') as f:
            pickle.dump(content, f, pickle.HIGHEST_PROTOCOL)


def cache_read(arg):
    if arg in ["post", "category"]:
        path = os.path.join(ROOT, 'cache', arg + '.pkl')
        with open(path, 'rb') as f:
            pages = pickle.load(f)
        return pages


def get_articles_from_pages(pages):
    return list(map(lambda x: x.open_article(), pages))


def get_file_info(path):
    file = os.path.basename(path)
    splitext = os.path.splitext(file)
    if splitext[1] in [".md", ".markdown"]:
        return splitext[0], "markdown"
    elif splitext[1] == ".html":
        return splitext[0], "html"


def is_post(path):
    file = os.path.basename(path)
    splitext = os.path.splitext(file)
    if splitext[0] in USER_CONFIG["pages"]:
        return False
    else:
        return True


def insert_article(article, article_list):
    for index, value in enumerate(article_list):
        if value['date'] < article['date']:
            article_list.insert(index, article)
            break
    return article_list


def pagination_pages(articles):
    # pwa
    filename = 'posts.json'
    pagination_path = os.path.join(OUTPUT_PATH, 'data', filename)
    content = json.dumps(articles)
    writer(pagination_path, content)
    """
    for i in range(int(num + 1)):
        filename = 'page-' + str(i + 1) + '.html'
        pagination_path = os.path.join(OUTPUT_PATH, filename)
        start = i * SITE_DEFAULT_CONFIG['pagination_number']
        next = (i + 1) * SITE_DEFAULT_CONFIG['pagination_number']
        end = -1 if next > length else next
        content = wrap_template_render('list.jinja', articles=articles[start, end])
        writer(pagination_path, content)
    """


def category_pages(categories):
    filename = 'categories.json'
    category_path = os.path.join(OUTPUT_PATH, 'data', filename)
    content = json.dumps(categories)
    writer(category_path, content)


def generate_page(pages):
    for page in pages:
        print("Writing: ", page.generate_json_file())


def generate_config(**kw):
    config_src = os.path.join(ROOT, 'rosta', 'config.jinja')
    config_dst = os.path.join(ROOT, 'rosta', 'config.py')
    template_dir = os.path.join(ROOT, 'templates')
    content = reader(config_src)
    template = jinja2.Template(content)
    writer(config_dst, template.render(
        site_name=kw['site_name'],
        description=kw['description'],
        name=kw['name'],
        template_dir=template_dir,
        output_path=kw['output_path'],
        origin_path=kw['origin_path'],
        remote_url=kw['remote_url']
    ))


def generate_app():
    # router build
    app_file = os.path.join(ROOT, 'site', 'src', 'App.vue')
    index_file = os.path.join(ROOT, 'site', 'index.html')
    build_file = os.path.join(ROOT, 'site', 'config', 'index.js')
    router_file = os.path.join(ROOT, 'site', 'src', 'router', 'index.js')
    app_content = wrap_template_render('App.jinja')
    router_content = wrap_template_render('router.jinja')
    index_content = wrap_template_render('index.jinja')
    build_content = wrap_template_render('build_config.jinja', output_path=OUTPUT_PATH)
    writer(app_file, app_content)
    writer(index_file, index_content)
    writer(build_file, build_content)
    writer(router_file, router_content)


def run_command(command):
    process = subprocess.Popen(shlex.split(command), stdout=subprocess.PIPE)
    while True:
        output = process.stdout.readline()
        if (output == b'') and (process.poll() ==0):
            break
        if output:
            print(output.strip().decode())
    rc = process.poll()
    if rc != 0:
        raise OSError
    else:
        return rc


def build():
    if not subprocess.check_call('npm -v', shell=True):
        app_file_name = os.path.join(ROOT, 'site')
        os.chdir(app_file_name)
        run_command('cnpm install')
        run_command('npm run build')
    else:
        print('Command `npm` not found.')


def create_repo(remote):
    # two branches
    # refer: http://pressedpixels.com/articles/deploying-to-github-pages-with-git-worktree/
    repo = Repo.init(OUTPUT_PATH)
    if remote:
        origin = repo.create_remote('origin', GIT_REPO)
    git = repo.git
    git.commit('--allow-empty', '-m', "Initial master")
    git.checkout('--orphan', 'source')
    git.reset()
    git.commit('--allow-empty', '-m', "Initial commit")
    git.checkout('--force', 'master')
    git.worktree('add', ORIGIN_PATH, 'source')
    # if remote:
    #    origin.push('--all')
    return repo


def get_repo():
    master = Repo(OUTPUT_PATH)
    source = Repo(ORIGIN_PATH)
    return (master, source)


def get_update_info(repo, path):
    info = []
    # 1. new
    new_files = repo.untracked_files
    # TODO: file is unicode, so i have to 2to3

    # 2. modified
    modified_files = repo.git.diff('HEAD~1..HEAD', name_only=True)

    info = new_files + modified_files.split('\n')
    if path == "site":
        result = list(map(lambda x: os.path.join(OUTPUT_PATH, x), info))
    elif path == "source":
        result = list(map(lambda x: os.path.join(ORIGIN_PATH, x), info))
    return result


def files_tobe_commit():
    master, source = get_repo()
    # files = get_update_info(master, "site") + get_update_info(source, "source")
    files = get_update_info(source, "source")
    return files


def commit_change():
    # commit
    # repo.head master / gh-pages
    # repo.git.diff('HEAD~1..HEAD', name_only=True)
    master, pages = get_repo()
    master.git.add('--all')
    pages.git.add('--all')
    master.git.commit('-m', date.today().isoformat())
    pages.git.commit('-m', date.today().isoformat())


def remote_publish():
    # repo.remotes.origin.push(new_tag)
    master, _ = get_repo()
    try:
        master.git.push('--all', 'origin')
        print("Success.")
    except Exception as e:
        print(e)


def reader(path):
    with open(path, encoding='utf-8') as f:
        file_buffer = f.read()
    return file_buffer


def writer(path, content):
    try:
        with open(path, 'w', encoding='utf-8') as output_file:
            output_file.write(content)
        return True
    except Exception as e:
        print("Error happened while writing file:", e)
        return False


def jinja2_setup(env):
    template_environment = jinja2.Environment(
        **env
    )
    return template_environment


def wrap_template_render(target, **kw):
    env = SITE_DEFAULT_CONFIG['jinja2_env']
    template_environment = jinja2_setup(env)
    template = template_environment.get_template(target)
    return template.render(site_config=USER_CONFIG, **kw)

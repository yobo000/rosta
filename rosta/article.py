# -*- coding: utf-8 -*-
"""
实现markdown的编译
md读第一个空行 取meta信息
"""

from utils import reader, compile_article_content


class Article(object):
    """docstring for Article"""
    # 返回文章对象供render
    # 现在是md文件就先用md_file_path
    def __init__(self, file_path):
        super(Article, self).__init__()
        self.file_path = file_path
        self.metadata = {}

    def get_content(self):
        content = reader(self.file_path)
        article = compile_article_content(content)
        return article

    def read_metadata(self):
        # metadata
        # 在python-markdown中已经集成了meta拓展所以若要支持这些extension需对paka.cmark进行修改
        pass

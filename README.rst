=====
rosta
=====


.. image:: https://img.shields.io/pypi/v/rosta.svg
        :target: https://pypi.python.org/pypi/rosta

.. image:: https://img.shields.io/travis/yobo000/rosta.svg
        :target: https://travis-ci.org/yobo000/rosta

.. image:: https://readthedocs.org/projects/rosta/badge/?version=latest
        :target: https://rosta.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://pyup.io/repos/github/yobo000/rosta/shield.svg
     :target: https://pyup.io/repos/github/yobo000/rosta/
     :alt: Updates



A Static Website Generator


* Free software: BSD license
* Documentation: https://rosta.readthedocs.io.

Roadmap
--------
* Remove the pure html generator
* Render vue file

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
